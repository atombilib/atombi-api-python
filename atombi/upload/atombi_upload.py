import boto3
from botocore.exceptions import NoCredentialsError


def upload(bucket: str, target: str, access: str, secret: str, file):
    """
    Carrega dados no s3
    :param bucket: nome do bucket da s3
    :param target: caminho para o arquivo
    :param access: chave de acesso
    :param secret: segredo do acesso
    :param file: Arquivo que deve ser carregado. Para fim de uso no PowerBI, os arquivo deve conter todos os dados,
    ou seja, um unico arquivo para todos os dados
    :return:
    """
    s3 = boto3.client('s3', aws_access_key_id=access,
                      aws_secret_access_key=secret)

    try:
        s3.upload_file(file, bucket, target)
        print("Upload Successful")
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False
