import uuid
import jwt
from datetime import datetime as DateTime
import base64


def get_jwt_token(domain:str, public_key:str, secret_key:str):
    """
        Gera um token JWT para autenticação nas API's do AtomBI.
        Esse token deve ser enviado com a parametro X-TOKEN-JWT

    :param domain: Domínio da conta registrado no AtomBi
    :param public_key: Chave publica do token de acesso de um usuário
    :param secret_key: Chave privada do token de acesso de um usuário
    """
    now = DateTime.now().timestamp()
    exp = now + 180
    jti = str(uuid.uuid4())
    encoded_key = string_to_base64(secret_key)

    payload = {'jti': jti,
               'sub': public_key,
               'iat': str(now).split('.')[0],
               'exp': str(exp).split('.')[0],
               'nbf': str(now).split('.')[0],
               'iss': domain
               }

    token = jwt.encode(payload, encoded_key, algorithm='HS512', headers=None)
    temp_token = str(token)
    token = 'Bearer ' + temp_token[2:len(temp_token) - 1]

    return token


def string_to_base64(s):
    return base64.b64encode(s.encode('UTF-8'))

